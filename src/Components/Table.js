// const [data, setData] = useState([])
// useEffect(() => {
//   const doFetch = async () => {
//     const response = await fetch("https://randomuser.me/api/?results=100")
//     const body = await response.json()
//     const contacts = body.results
//     console.log(contacts)
//     setData(contacts)
//   }
//   doFetch()
// }, [])


import React, { useEffect, useState } from 'react'
import styled from 'styled-components'
import DataTable from './tableData';

import axios from 'axios'

const Styles = styled.div`
  padding: 1rem;
  thead {
      background-color:#6F8392
  }

  table {
    border-spacing: 0;
    border: 1px solid black;

    tr {
      :last-child {
        td {
          border-bottom: 0;
        }
      }
      :nth-child(even) {
        background-color: Lightgrey;
    }
    }

    th,
    td {
      margin: 0;
      padding: 0.5rem;
      border-bottom: 1px solid black;
      border-right: 1px solid black;

      :last-child {
        border-right: 0;
      }
    }
  }
`
const EditableCell = ({
    value: initialValue,
    row: { index },
    column: { id },
    updateMyData, // This is a custom function that we supplied to our table instance
  }) => {
    // We need to keep and update the state of the cell normally
    const [value, setValue] = React.useState(initialValue)
  
    const onChange = e => {
      setValue(e.target.value)
    }
  
    // We'll only update the external data when the input is blurred
    const onBlur = () => {
      updateMyData(index, id, value)
    }
  
    // If the initialValue is changed external, sync it up with our state
    React.useEffect(() => {
      setValue(initialValue)
    }, [initialValue])
  
    return <input value={value} onChange={onChange} onBlur={onBlur} />
  }
  

function App() {
   const [data, setData] = useState([]);
   const updateMyData = (rowIndex, columnId, value) => {
    // We also turn on the flag to not reset the page
    setData(old =>
      old.map((row, index) => {
        if (index === rowIndex) {
          return {
            ...old[rowIndex],
            [columnId]: value,
          }
        }
        return row
      })
    )
  }
// console.log(updateMyData());
  useEffect(() => {
        axios
        .get(`https://s3-ap-southeast-1.amazonaws.com/he-public-data/reciped9d7b8c.json`)
        .then((res)=>{
                if(localStorage.getItem('update_item')){
                    setData(JSON.parse(localStorage.getItem('update_item')))
                } else {
                    setData(res.data)
                }
                localStorage.setItem('api_data', JSON.stringify(res.data));
                // localStorage.setItem('update_item', JSON.stringify(res.data))
             }
            );
        }, [])
        
  const columns = React.useMemo(
    () => [
          {
            Header: 'Id',
            accessor: 'id',
            canGroupBy:false, 
            canSort:true           // Aggregate the average age of visitors
            // aggregate: 'average',
            // Aggregated: ({ value }) => `${Math.round(value * 100) / 100} (avg)`,
          },
          {
              Header:'name',
              accessor:'name',
              canGroupBy:false
          },
          {
            Header: 'category',
            accessor: 'category',
            // Aggregate the sum of all visits
            aggregate: 'uniqueCount',
            canSort:true,
            Aggregated: ({ value }) => `${value} Unique Names`,
          },

          {
              Header:'label',
              accessor:'label',
              canGroupBy:false,
              canSort:true 
          },
          {
              Header:'price',
              accessor:'price',
              canGroupBy:false,
              canSort:false ,
              Cell:EditableCell
          },
          {
            Header:'image',
            accessor:'image',
            canGroupBy:false,
            canSort:true 
          },
          {
              Header:'description',
              accessor:'description',
              canGroupBy:false,
              canSort:true
          },
      
    ],
    []
  )
  

//   const data = React.useMemo(() => makeData(10000), [])
console.log("kjikhk", data);
 
  return (
    <Styles>
        <div style={{display:'flex'}}>
            <button style={{margin:'5px', backgroundColor:'green', color:'white'}} onClick={()=>{
                localStorage.setItem('update_item', JSON.stringify(data))
            }}>save</button>
            <button style={{margin:'5px', backgroundColor:'grey', color:'white'}} onClick={()=>{
                setData(JSON.parse(localStorage.getItem('api_data')))
                localStorage.removeItem('update_item')
                // console.log("hhh",localStorage.getItem('api_data') );
            }}>Reset</button>
        </div>
      <DataTable columns={columns} data={data} updateMyData={updateMyData} setData={setData} />
    </Styles>
  )
}

export default App
